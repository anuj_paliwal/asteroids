Function.prototype.inherits = function (parent) {
  function Surrogate() {};
  Surrogate.prototype = parent.prototype;

  this.prototype = new Surrogate();
};

(function (root) {
  var Asteroids = root.Asteroids = (root.Asteroids || {});

  var Ship = Asteroids.Ship = function (pos) {
    Asteroids.MovingObject.call(this, pos);

    this.vel = [0,0];
    this.radius = 5;
    this.color = "black";
  };
  Ship.inherits(Asteroids.MovingObject);

  Ship.prototype.power = function(impulse) {
    this.vel = [this.vel[0] + impulse[0], this.vel[1] + impulse[1]];
  }

})(this);