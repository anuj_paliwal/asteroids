Function.prototype.inherits = function (parent) {
  function Surrogate() {};
  Surrogate.prototype = parent.prototype;

  this.prototype = new Surrogate();
};

(function (root) {
  var Asteroids = root.Asteroids = (root.Asteroids || {});

  var Asteroid = Asteroids.Asteroid = function (pos, vel) {
    COLOR = 'red';
    RADIUS = 75;

    Asteroids.MovingObject.call(this, pos, vel);
    this.color = COLOR;
    this.radius = RADIUS;
  };
  Asteroid.inherits(Asteroids.MovingObject);

  Asteroid.randomAsteroid = function (dimX, dimY) {
    var pos = [Math.random() * dimX, Math.random() * dimY];
    var vel = [Math.random() * 7, Math.random() * 7]; //may need to change vel
    return new Asteroid(pos, vel);
  };

  Asteroid.prototype.isOffScreen = function () {
    if (this.pos[0] < 0 || this.pos[0] > Asteroids.Game.DIM_X) {
      return true;
    } else if (this.pos[1] < 0 || this.pos[1] > Asteroids.Game.DIM_Y) {
      return true;
    } else {
      return false;
    }
  }

})(this);