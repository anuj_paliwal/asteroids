Function.prototype.inherits = function (parent) {
  function Surrogate() {};
  Surrogate.prototype = parent.prototype;

  this.prototype = new Surrogate();
};

(function (root) {
  var Asteroids = root.Asteroids = (root.Asteroids || {});

  var Game = Asteroids.Game = function (ctx) {
    this.ctx = ctx;
    this.asteroids = Game.addAsteroids(3 + Math.floor(Math.random() * 4));
    this.ship = new Asteroids.Ship([Game.DIM_X/2, Game.DIM_Y/2]);
  };

  Game.DIM_X = 800;
  Game.DIM_Y = 600;
  Game.INTERVAL = 33;

  Game.addAsteroids = function(num) {
    var arr = [];
    for(var i = 0; i < num; i++) {
      arr.push(Asteroids.Asteroid.randomAsteroid(Game.DIM_X, Game.DIM_Y));
    }
    return arr;
  }

  Game.prototype.draw = function () {
    this.ctx.clearRect(0, 0, Game.DIM_X, Game.DIM_Y);
    this.ship.draw(this.ctx);
    _.each(this.asteroids, function(asteroid) {
      asteroid.draw(this.ctx);
    });
  };

  Game.prototype.move = function() {
    this.ship.move();
    _.each(this.asteroids, function(asteroid) {
      asteroid.move();
    });
  };

  Game.prototype.checkCollisions = function () {
    var game = this;
    _.each(this.asteroids, function(asteroid) {
      if(asteroid.isCollidedWith(game.ship)) {
        game.stop();
        alert("You dead");
      }
    });
  };

  Game.prototype.removeOffScreen = function () {
    var game = this;
    _.each(this.asteroids, function(asteroid) {
      if (asteroid.isOffScreen()) {
        var i = game.asteroids.indexOf(asteroid);
        game.asteroids.splice(i, 1);
      }
    });
  };

  Game.prototype.step = function () {
    this.move();
    this.draw();
    this.checkCollisions();
    this.removeOffScreen();
    console.log(this.asteroids.length);
  };

  Game.prototype.start = function () {
    var game = this;
    this.timer = setInterval(function () {
      game.step();}, Game.INTERVAL);
  };

  Game.bindKeyHandlers = function () {

  }

  Game.prototype.stop = function () {
    clearInterval(this.timer);
  }

})(this);